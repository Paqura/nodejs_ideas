/**
 * * Функция проверка - зареген ли юзер
 */

module.exports = {
  ensureAuthenticated: function(req, res, next) {
    if(req.isAuthenticated()) {
      return next();
    }
    req.flash('error_msg', 'Авторизуйся');
    res.redirect('/users/login');
  }
}