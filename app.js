const express = require("express");
const path = require("path");
const passport = require("passport");
const session = require("express-session");
const flash = require("connect-flash");
const methodOverride = require("method-override");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = express();

/**
 * * Если dev-mode 8080
 */
const port = process.env.PORT || 8080;

/**
 * * Загрузка роутов
 */

const ideas = require("./routes/ideas");
const users = require("./routes/users");

/**
 * * Passport config
 */

require("./config/passport")(passport);

/**
 * * db config
 */

const db = require("./config/database");

/**
 * * Подключаем mongoose
 */

mongoose.PromiseProvider = global.Promise;

/**
 * @param  Uri  из конфига
 * */

mongoose
  .connect(db.mongoURI)
  .then(() => console.log("MongoDB connected"))
  .catch(err => console.log(err));

/**
 * * handlebars middleware
 */

app.engine(
  "handlebars",
  exphbs({
    defaultLayout: "main"
  })
);
app.set("view engine", "handlebars");

/**
 * * session middleware
 */

app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);

/**
 * ! паспорт мидлвары, обязательно после сессии экспресса
 */

app.use(passport.initialize());
app.use(passport.session());

/**
 * * подключаем уведомления
 */

app.use(flash());

/**
 * * Глобальные переменные
 */

app.use(function(req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

// * bodyparser middleware

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * * Теперь пути в файлах будут начинаться с '/'
 * * если они находятся в папке public
 */

app.use(express.static(path.join(__dirname, "public")));

/**
 * * Переписывает существующие методы
 * * используя свои хаки в шаблонах
 */

app.use(methodOverride("_method"));

app.get("/", (req, res) => {
  const title = "Создавай задачи и выполняй их";
  res.render("index", {
    title
  });
});

app.get("/about", (req, res) => {
  res.render("about");
});

/**
 * * используем импортированные модули
 * * передавая им базовый роут, чтобы
 * * не писать лишние пути
 */

app.use("/ideas", ideas);
app.use("/users", users);

app.listen(port, () => {
  console.log(`started on ${port} port`);
});
