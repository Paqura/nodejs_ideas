class Filter 
{
  constructor() 
  {
    this.categories = Array.from(document.querySelectorAll(".category")) || [];
    this.select = document.querySelector(".category-select") || null;
    if (this.select) {
      this.select.addEventListener("change", evt => {
        const target = evt.target;
        this.changeSelectedItem(target);
      });
      this.renderFilterList();
    }
  }

  changeSelectedItem(target) 
  {
    const category = target
      .options[target.selectedIndex]
      .value
      .toLowerCase();

    this.showFilteredList(category);
  }

  showFilteredList(category) 
  {
    const showAll = 'все';
    [].forEach.call(this.categories, item => {
      if(category === showAll) {
        return item.closest(".card").style.display = "flex";
      }
      return item.dataset.category.toLowerCase() === category
        ? (item.closest(".card").style.display = "flex")
        : (item.closest(".card").style.display = "none");
    });
  }
  renderFilterList() 
  {
    const filtered = this.categories
      .map(it => it.textContent)
      .filter((it, i, a) => a.indexOf(it) === i);
    filtered.forEach(it => {
      let option = document.createElement("option");
      option.setAttribute("value", it);
      option.textContent = it;
      this.select.appendChild(option);
    });
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const filter = new Filter();
});
