const LocalStrategy = require("passport-local").Strategy;
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

// * загружаем модель юзера

const User = mongoose.model("users");

module.exports = function(passport) {
  passport.use(
    new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
      // * проверяем юзера
      User.findOne({
        email
      }).then(user => {
        if (!user) {
          return done(null, false, { message: "Нет такого юзера, лала" });
        }
        // * проверяем пароль
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if(err) throw err;
          if(!isMatch) {
            return done(null, false, { message: "Неправильный пароль" });
          } else {
            return done(null, user);
          }   
        });
      });
    })
  );

  /**
   * * Стандартный метод сери/десери/ализации из паспорта
   */
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
