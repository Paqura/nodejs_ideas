/**
 * * Проверяем окружение и 
 * * в зависимости от него пробрасываем 
 * * местоположение базы
 */

if(process.env.NODE_ENV === 'production') {
  module.exports = { mongoURI: 'mongodb://slavals:paqura616@ds159489.mlab.com:59489/vidjot-prod' }
} else {
  module.exports = { mongoURI: 'mongodb://localhost/vidjot' }
}