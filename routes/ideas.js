const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const {ensureAuthenticated} = require('../helpers/auth');

// load model
require("../models/Idea");
const Idea = mongoose.model("ideas");

router.get('/', ensureAuthenticated, (req, res) => {
  Idea.find({user: req.user.id})
    .sort({date: 'desc'})
    .then(ideas => {
      res.render('ideas/index', {
        ideas
      });
    })  
});

router.get("/add", ensureAuthenticated, (req, res) => {
  res.render("ideas/add");
});

router.get('/edit/:id', ensureAuthenticated, (req, res) => {
  Idea.findOne({
    _id: req.params.id
  })
    .then(idea => {
      if(idea.user !== req.user.id) {
        req.flash('error__msg', 'Не ваш аккаунт');
        res.redirect('/ideas');
      } else {
        res.render('ideas/edit', {
          title: idea.title,
          description: idea.description,
          id: idea.id,
          category: idea.category
        });
      }      
    })  
});

router.put('/:id', ensureAuthenticated, (req, res) => {
  Idea.findOne({
    _id: req.params.id
  })
    .then(idea => {
      idea.title = req.body.title;
      idea.description = req.body.description;
      idea.category = req.body.category || '';
      idea.save()
        .then(() => {
          req.flash('success_msg', 'Задача изменена, тащи новые')
          res.redirect('/ideas');
      })
    })
});

router.post("/",  ensureAuthenticated, (req, res) => {
  let errors = [];

  if (!req.body.title) errors.push({ text: "Заполнить заголовок было бы неплохо" });
  if (!req.body.description) errors.push({ text: "Заполнить описание было бы неплохо" });
  if (!req.body.category) errors.push({ text: "Добавить категорию, чтобы было легче искать" });

  if (errors.length > 0) {
    res.render("ideas/add", {
      errors,
      title: req.body.title,
      description: req.body.description,
      category: req.body.category
    });
  } else {
    const newUser = {
      user: req.user.id,
      title: req.body.title,
      description: req.body.description,
      category: req.body.category
    };
    new Idea(newUser)
      .save()
      .then(idea => {
        req.flash('success_msg', 'Задача добавлена')
        res.redirect('/ideas')
      })
  }
});

router.delete('/:id', ensureAuthenticated, (req, res) => {
  Idea.deleteOne({
    _id: req.params.id
  })
    .then(() => {
      req.flash('success_msg', 'Задача удалена, тащи новые')
      res.redirect('/ideas')
    })
})

module.exports = router;