const express = require("express");
const bcrypt = require("bcryptjs");
const passport = require('passport');
const router = express.Router();
const mongoose = require("mongoose");

// load model

require("../models/User");
const User = mongoose.model("users");

router.get("/login", (req, res) => {
  res.render("users/login");
});

router.get("/register", (req, res) => {
  res.render("users/register");
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/ideas',
    failureRedirect: '/users/register',
    failureFlash: true
  })(req, res, next)
});

router.post("/register", (req, res) => {
  let errors = [];

  if (req.body.password.length < 5)
    errors.push({ text: "Пароль слишком короткий" });

  if (errors.length > 0) {
    res.render("users/register", {
      errors,
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    });
  } else {
    User.findOne({ email: req.body.email }).then(user => {
      if (user) {
        req.flash("error_msg", "Эта почта уже забита");
        res.redirect("/users/register");
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password.toString()
        });
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash(
                  "success_msg",
                  "Регистрация закончилась, можешь входить"
                );
                res.redirect("/users/login");
              })
              .catch(err => console.error(err));
          });
        });
      }
    });
  }
});

router.get('/logout', (req, res) => {
  req.logOut();

  req.flash('success_msg', 'Зачем вышел? Ну ладушки...');
  res.redirect('/users/login');
})

module.exports = router;
